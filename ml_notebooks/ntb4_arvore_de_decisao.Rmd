---
title: "R Notebook 5 - Árvore de decisão"
output: 
  html_document: 
    css: 'www/estilos.css'
    self_contained: yes
    toc: true
    toc_depth: 2
    toc_float: TRUE
---

```{r, include=FALSE}
# Definições gerais para os *chunks*
knitr::opts_chunk$set(echo = TRUE, warning = FALSE, message = FALSE)
```


> Árvores de decisão são fáceis de interpertar, pois fornecem as **regras de negócio** necessárias para classificar os dados e fornecer uma predição.


Algumas das características são:

- Composta por **nós**, em que os extremos são:
  - raiz (primeiro) e
  - folhas (últimos, onde o resultado final de um conjunto de decisões é mostrado).
  
Lendo os pacotes que serão utilizados:

```{r}
# Pacotes a serem utilizados neste post
library(dplyr)
library(readr)
library(rpart)
library(rpart.plot)
library(ggplot2)
```

  
  
Na sequência iremos apresentar duas análises com árvores de decisão: a primeira para um conjunto de dados de *turnover* de colaboradores; a segunda para um conjunto de dados da tragédia do navio Titanic. 

# Árvore para Attrition

## Lendo os dados

```{r}

df <- 
  read_csv("../data/attrition.csv") %>% 
  mutate_if(is.character, as.factor) 

```

## Dividindo em treino e teste

```{r}
## set the seed to make your partition reproducible
set.seed(123)

# Create training subset
train <- df %>% sample_frac(.70)
# Create test subset
test  <- anti_join(df, train)
```



## Construindo a árvore nos dados de treino

```{r}

rtree_fit <- 
  rpart(
    Attrition ~ .,
    train
  )

rpart.plot(rtree_fit)

```


## Definindo ponto de poda para a árvore de decisão

```{r}
printcp(rtree_fit)

plotcp(rtree_fit)

```


Poda (*prune*) na árvore:

```{r}
rtree_fit <- 
  prune(rtree_fit, cp = 0.021)

rpart.plot(rtree_fit)  
```

### Importância das variáveis


```{r}
ggplot(
  stack(rtree_fit$variable.importance) %>% 
    arrange(desc(values)), 
  aes(x=reorder(ind,values), y=values,fill=values)) +
   geom_bar(stat="identity", position="dodge")+ coord_flip()+
   ylab("Variable Importance")+
   xlab("")+
   ggtitle("Information Value Summary")+
   guides(fill=F)+
   scale_fill_gradient(low="red", high="blue")
```


## Fazendo as predições

Aplicando a função `preditc()`.

```{r}
pred_num <- predict(rtree_fit,  newdata= test)[,2] # segunda coluna para pegar apenas survived = 1
pred <- predict(rtree_fit,  newdata= test, type="class")
```

Matriz confusão pelo pacote `caret`:

```{r}
caret::confusionMatrix(pred, test$Attrition)
```

## ROC Curve

Veja a curva ROC do modelo estimado.

```{r}
library(pROC)

pred_roc <- 
  data.frame(pred_num, as.factor(as.numeric(test$Attrition)-1)) %>% arrange(desc(pred_num))
colnames(pred_roc) <- c("pred", "attrition")

roc <- roc( test$Attrition , pred_roc$pred)

ggroc(roc, legacy.axes = TRUE, colour = "blue", size = 1)+
  geom_abline(linetype = "dashed" )

```


## Considerações

O modelo de árvore de decisão não se adequou bem na realização de predições para o conjunto de dados de *turnover*. As predições apresentaram uma grande quantidade de erros quando o algoritmo disse que o colaborador não sairia, mas ele saiu. Neste caso este é o pior tipo de erro que poderia ocorrer no ponto de vista da empresa. Além disso, a curva ROC ficou muito próxima à diagonal sinalizada nos gráficos, o que significa que o modelo ficou próximo à escolhas aleatórias.













# Árvore para dados do Titanic

## Lendo o conjunto de dados do Titanic

Vamos buscar o arquivo csv do repositório. Após iremos excluir as variáveis a não serem utilizadas e separar o dataset em treino e teste.

```{r}

df_titanic_tree <- 
  read_csv("https://gitlab.com/dados/open/raw/master/titanic.csv") %>% 
  mutate_if(is.character, as.factor) %>% 
  select(-name, -ticket, -embarked, -cabin, - passengerid) %>% 
  mutate(survived = as.factor(survived))

df_titanic_tree %>% 
  head(5) 
```


Na sequência segue o dicionário dos dados:

- `survived`: sobreviveu $= 1$; não sobreviveu $=0$;
- `pclass`: classe (1, 2 e 3, indicando primeira, segunda e terceira classe);
- `sex`: masculino (*male*) e feminino (*female*);
- `age`: idade do passageiro;
- `sibsp`: número de irmãos e cônjuge (*siblings and spouse*) a bordo;
- `parch`: número de pais e filhos (*parents and children*) a bordo;
- `fare`: tarifa paga pelo passageiro.


## Criando uma árvore de decisão com todo o *dataset*

Primeiro, apenas para fins didáticos, vamos contruir uma árvore de decisão para todo o *dataset*, ou seja, não iremos considerar o processo de *machine learning* de treinar e testar o modelo.

Na árvore a seguir, cada nós mostra:
- a predição binária da classe (não sobreviveu = 0; sobreviveu = 1);
- a probabilidade predita de sobrevivência (de 0 a 1);
- a porcentagem de observações no nós.

```{r}
rtree_fit <-
  rpart(survived ~ ., 
          df_titanic_tree)

rpart.plot(rtree_fit)

```


## Divindo em treino e teste

Agora sim, iremos dividir o conjunto de dados em treino e teste.

```{r}
# Separar os dados em treino e teste
set.seed(100)
.data <- c("training", "test") %>%
  sample(nrow(df_titanic_tree), replace = T) %>%
  split(df_titanic_tree, .)
```

Vamos agora criar a árvore de decisão nos dados de treino:

```{r}
# Criar a árvore de decisão
rtree_fit <- rpart(survived ~ ., 
          .data$training
          )
rpart.plot(rtree_fit)

```


**EXERCÍCIO**: Troque o seed para outros valores aleatórios e verifique como a estrutura da árvore muda.


## Fazendo as predições

Aplicando a função `preditc()`.

```{r}
pred_num <- predict(rtree_fit,  newdata= .data$test)[,2] # segunda coluna para pegar apenas survived = 1
pred <- predict(rtree_fit,  newdata= .data$test, type="class")
```

Matriz confusão:

```{r}
table(pred, .data$test$survived )
```

Pelo pacote `caret`:

```{r}
caret::confusionMatrix(pred, .data$test$survived)
```

## ROC Curve

Veja a curva ROC do modelo estimado.

```{r}
library(pROC)

pred_roc <- 
  data.frame(pred_num, as.factor(as.numeric(.data$test$survived)-1)) %>% arrange(desc(pred_num))
colnames(pred_roc) <- c("pred", "survived")

roc <- roc(pred_roc$survived , pred_roc$pred)

ggroc(roc, legacy.axes = TRUE, colour = "blue", size = 1)+
  geom_abline(linetype = "dashed" )
```


## Fontes

- Detalhes sobre árvores de decisão no `R`:
  - <http://www.milbo.org/doc/prp.pdf>
