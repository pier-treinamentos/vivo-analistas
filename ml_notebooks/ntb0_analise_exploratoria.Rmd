---
title: "Análise exploratória: univariada e bivariada"
output: 
  html_document: 
    css: 'www/estilos.css'
    self_contained: yes
    toc: true
    toc_depth: 2
    toc_float: TRUE
---

```{r, include=FALSE}
# Definições gerais para os *chunks*
knitr::opts_chunk$set(echo = TRUE, warning = FALSE, message = FALSE)
```

A **análise exploratória** é o uso de **técnicas estatísticas** para **resumir os dados** e viabilizar **conclusões rápidas**, frequentemente com uso de **tabelas e gráficos**.

Etapas da análise exploratória:

1. **Univariada**: analisa **uma variável por vez**, geralmente olhando para medidas-resumo e para as distribuições.
2. **Bivariada**: faz o cruzamento de **duas em duas variáveis**, geralmente olhando para como estas variáveis **oscilam de forma conjunta**.

Vamos então carregar os pacotes a serem utilizados e definir configurações gerais:

```{r}
# Pacotes a serem utilizados neste post
library(dplyr)
library(readr)
library(ggplot2)
library(purrr)
library(highcharter)
library(htmltools)
library(tidyr)

# Definindo paleta de cores para os gráficos
aux <- list()
aux$ey_colors <- c("#797878", "#ffcb05", "#3466af", "#999999", "#003366", "#cccccc")

```


## Lendo os dados

Primeiro iremos ler o conjunto de dados a ser analisado. Adicionalmente iremos corrigir os tipos de variáveis que foram inferidos erroneamente na leitura pela linguagem `R`.

```{r}

df <- 
  read_csv("../data/attrition.csv") %>% 
  mutate_if(is.character, as.factor) %>% # Mudando tipos de variáveis 
    mutate(
    Education = as.factor(Education),
    EmployeeCount = as.factor(EmployeeCount),
    EnvironmentSatisfaction = as.factor(EnvironmentSatisfaction),
    JobInvolvement = as.factor(JobInvolvement),
    JobLevel = as.factor(JobLevel),
    JobSatisfaction = as.factor(JobSatisfaction),
    PerformanceRating = as.factor(PerformanceRating),
    RelationshipSatisfaction = as.factor(RelationshipSatisfaction),
    StockOptionLevel = as.factor(StockOptionLevel)
  )

```


Verificando a estrutura das variáveis numéricas:

```{r}
# Identificando o tipo das variáveis 
df %>% select_if(is.numeric) %>%  str()
```


Verificando a estrutura das variáveis categóricas (fatores):

```{r}
# Identificando o tipo das variáveis 
df %>% select_if(is.factor) %>% str()
```


## Análise univariada

### Variáveis numéricas


A seguir, calculamos as estatísticas descritivas para as variáveis numéricas:

```{r}

df %>% select_if(is.numeric) %>% 
  gather(var, valor) %>% 
  group_by(var) %>% 
  dplyr::summarise(
    mean = mean(valor),
    sd = sd(valor),
    min = min(valor),
    q1 = quantile(valor, 1/4), 
    median = median(valor),
    q3 = quantile(valor, 3/4),
    max = max(valor)
  ) 
```


Seguem os histogramas das variáveis numéricas:

```{r}

df_num <-
  df %>% select_if(is.numeric)

map(names(df_num), function(x){
  df_num[[x]] %>% 
    hchart(showInLegend = FALSE) %>% 
    hc_add_theme(hc_theme_smpl()) %>% 
        hc_title(text = x) %>% 
        hc_colors('#ffcb05') %>%
        hc_chart(zoomType = "xy") %>%
        hc_yAxis(title = list(text = ""))
  }) %>% 
  hw_grid(rowheight = 225, ncol = 2) %>% browsable()


```



### Variáveis categóricas


Para as variáveis categóricas não conseguimos calcular as mesmas estatísticas descritivas que fazemos para as variáveis numéricas. Uma opção, no entanto, é fazer a contagem dos dos valores e mostrar as classes mais frequentes em cada categoria. Veja a seguir:

```{r}

df %>% select_if(is.factor) %>%
  gather(var, valor) %>%
  group_by(var, valor) %>%
  dplyr::summarise(percent_moda = round(n()/nrow(df)*100),2) %>%
  ungroup %>%
  #mutate_if(is.character, as.factor) %>%
  group_by(var) %>%
  filter(percent_moda == max(percent_moda)) %>%
  left_join(
    df %>%
      select_if(is.factor) %>% 
      gather(var, valor) %>%
      group_by(var) %>%
      dplyr::summarise(levels = n_distinct(valor))
  ) %>% 
  select(var, levels, valor, percent_moda) %>% 
  rename(
    "Variável" = var,
    "Níveis" = levels,
    "Moda" = valor,
    "% da moda" = percent_moda
  )

```



Na sequência, os gráficos de colunas com as frequências dos fatores nas variáveis categóricas:

```{r}
df_factors <- 
  df %>% select_if(is.factor)

map(names(df_factors), function(x){
  df_factors[[x]] %>% 
    hchart(showInLegend = FALSE) %>% 
    hc_add_theme(hc_theme_smpl()) %>% 
        hc_title(text = x) %>% 
        hc_colors('#333') %>%
        hc_chart(zoomType = "xy") %>%
        hc_yAxis(title = list(text = ""))
  }) %>% 
  hw_grid(rowheight = 225, ncol = 2) %>% browsable()

```

## Análise bivariada

Vimos como analisar uma única variável por vez. Porém, em boa parte dos casos queremos observar **como uma variável se comporta em relação à outra**.

<br /> <br />

Estas relações entre variáveis podem ocorrer relacionando:

- Variáveis **numéricas e numéricas**: comparamos médias, distribuições, *outliers*, etc;
- Variáveis **numéricas e categóricas**: comparamos médias, distribuições, etc entre as classes de uma variável categórica;
- Variáveis **categóricas e categóricas**: comparamos proporções, por meio de contagens de ocorrências entre as classes.


### Relações entre variáveis numéricas


> Quando duas variáveis não são independentes (possuem alguma relação), geralmente é interessante analisar a **força da relação** entre elas. Para isso, podemos utilizar as métricas **covariância** e **correlação**.



#### Matriz de correlação


**Conceito**

- Uma matriz de correlação mostra todas as combinações de coeficientes de correlação entre as variáveis de um conjunto de dados. 
- A diagonal principal da matriz sempre apresentará valores igual a 1 (correlação da variável com ela mesma).
- Portanto, tanto a parte superior à diagonal da matriz quanto a inferior apresentam os mesmos coeficientes de correlação. 

<!-- - Aplicar um *heatmap* facilita a visualização de uma grande quantidade de coeificentes, sendo que os tons das cores indicam a força da correlação e a direção (se positiva ou negativa). -->

**São úteis para**

- Comprreender relações bivariadas no conjunto de dados.


```{r}

corr_mtx <-
  cor(
    df %>% select_if(is.numeric) %>%
      select(
        Age,
        MonthlyIncome,
        YearsAtCompany,
        #PercentSalaryHike,
        #PerformanceRating,
        TotalWorkingYears
      ) %>%
      na.omit,
    use = "complete.obs"
  )

corr_mtx
# corr_mtx %>% 
#   round(2) %>% 
#   knitr::kable(format = "markdown") %>%
#   kable_styling(full_width = F)
  

```

A seguir, a matriz de correlação é apresentada na forma de um *Heat Map* (mapa de calor):

```{r}
hchart(corr_mtx, animation = FALSE) %>%
 # hc_title(text = "Heatmap da matriz de correlação") %>%
  hc_size(height = 450)
```

### Exemplos de relações entre variáveis numéricas e categóricas 

Este tipo de análise faz uma mescla entre variáveis numéricas e categóricas. 

Gráfico de dispersão relacionando o salário mensal com a idade, com cores identificando o Departamento.

```{r}
hchart(
  df %>% filter(Department != "Human Resources") %>% sample_frac(.4) ,
  "scatter",
  hcaes(x = "Age", y = "MonthlyIncome" , group = "Department"),
  animation = FALSE
) %>%
  hc_title(text = "Age vs Monthly Income") %>%
  hc_subtitle(text = "Relations by Employee's Departments") %>%
  hc_size(width = "100%", height = 450) #%>%
#hc_colors(aux$ey_colors) 
```


 Exemplos de como relacionar variáveis categóricas e numéricas com o uso de *box-plot*:
 
 
```{r}

hcboxplot(
  x = df$MonthlyIncome  ,
  var = df$Attrition,
  outliers = FALSE,
  animation = FALSE
) %>% 
  hc_chart(type = "column") %>%  # to put box vertical
  hc_size(width = "100%", height = 420) %>%
  hc_title(text = "Monthly Income (US$)") %>%
  hc_subtitle(text = "by Attrition (Yes or No)") %>%
  hc_colors(aux$ey_colors)

```




```{r}

hcboxplot(
  x = df$MonthlyIncome  ,
  var = df$Attrition,
  var2 = df$Department,
  outliers = FALSE,
  animation = FALSE
) %>% 
  hc_chart(type = "column") %>%  # to put box vertical
  hc_size(width = "100%", height = 420) %>%
  hc_title(text = "Monthly Income (US$)") %>%
  hc_subtitle(text = "by Attrition (Yes or No) and Department") %>%
  hc_colors(aux$ey_colors)

```

## Considerações 

Demonstramos aqui um exemplo de como realizar uma análise exploratória de dados utilizando a linguagem R. A partir deste tipo de análise, mesmo utilizando apenas medidas de estatística descritivas e métodos de visualização de dados, já é possível obter *insights* importantes, que irão auxiliar na definição das hipóteses e também a compreender o melhor tipo de modelagem a ser utilizada. 
