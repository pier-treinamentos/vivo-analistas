---
title: "R Notebook 2 - *Principal Component Analysis*"
output: 
  html_document: 
    css: 'www/estilos.css'
    self_contained: yes
    toc: true
    toc_depth: 2
    toc_float: TRUE
---

```{r, include=FALSE}
# Definições gerais para os *chunks*
knitr::opts_chunk$set(echo = TRUE, warning = FALSE, message = FALSE)
```


```{r}
library(dplyr)
library(ggplot2)
```


## Aplicando PCA no conjunto de dados *mtcars*

O conjunto de dados "*Motor Trend Car Road Tests*" foi extraído de uma revista automobilística de 1974 e contempla 10 variáveis que representam características ou métricas de desempenho dos veículos. Os dados são nativos do R, basta digitar `mtcars` no console. Pra informações, como dicionário de dados, basta digitar `? mtcars`, e accessar a documentação de ajuda.

Veja o *head* dos dados:

```{r}
mtcars[,c(1:7,10,11)] %>% head(5)
```

Aplicando a função `prcomp`, nativa do R Stats, a qual executa uma análise dos componentes principais.

```{r}
mtcars.pca <- 
  prcomp(mtcars[,c(1:7,10,11)], center = TRUE, scale. = TRUE)
```

Cada análise de PCA resulta em um número de componentes principais igual ao número de variáveis. Porém, não pode-se dizer que um PCA é análogo a uma determinada variável.

A seguir, é apresentado resumo da importância de cada um dos componentes principais identificados.

```{r}
summary(mtcars.pca)$importance 
```

Veja que os 4 primeiros componentes principais já são responsáveis por 95% da porporção da variância dos dados.

Segue o `head` dos 4 primeiros componentes principais:

```{r}
mtcars.pca$x[,1:4] %>% head(10) 
```


## Visualizando graficamente os resultados do PCA

É possível também criar um *biplot* para visualizar como cada variável impacta nas relações entre os componentes principais.

Para isso, precisamos instalar e carregar um pacote que não está no CRAN (repositório oficial do R).

```{r}
## Instalando o pacote necessário
# install.packages('devtools') # Para instalar pacotes do GitHub.
# library(devtools)
# install_github("vqv/ggbiplot") 

# Carregando pacote instalado
library(ggbiplot)
```


Agora, veja o gráfico `biplot` que compara o PC1 e PC2 e mostra como cada variável impactou na construção de cada componente. Cada ponto do gráfico corresponde a uma linha de dado (dataset com 32 linhas no total). No gráfico

```{r}
# Criando Biplot para PC1 e PC2
ggbiplot(mtcars.pca, choices = 1:2)
```

Por fim, segue gráfico `biplot` comparando PC2 e PC3.

```{r}
# Criando Biplot para PC2 e PC3
ggbiplot(mtcars.pca, choices = 2:3)
```

## Considerações

O resultado de uma análise de componentes principais pode ser utilizado para estimar outros modelos. Veja, se você possui um *dataset* com um número muito grande de variáveis, o PCA viabiliza que haja uma redução de muitas variáveis para poucos componentes principais que expressem a maior parte da variância dos dados. A maior dificuldade desta abordagem é analisar os resultados depois, pois não há uma interpretação "real" para os valores dos componentes principais.
