library(readr)
library(dplyr)
library(highcharter)
library(corrplot)

# Lendo os dados da web
attrition <- read_csv("https://gitlab.com/dados/open/raw/master/ibm_hr_emplyee_attrition.csv") 

# Lendo os dados localmente
# attrition <- read_csv("data/attrition.csv") 

############################
# Covariância e correlação #
############################

# 1. Calcular a covariância e correlação anos totais de trabalho com salário mensal
cov(attrition$TotalWorkingYears, attrition$MonthlyIncome)

# 2. Calcular o coeficiente de correlação sem usar a função própria
cov(attrition$TotalWorkingYears, attrition$MonthlyIncome)/ 
  (sd(attrition$TotalWorkingYears)*sd(attrition$MonthlyIncome))

# 3. Usando a função de correlação do R
cor(attrition$TotalWorkingYears, attrition$MonthlyIncome)

########################
# MATRIZ DE CORRELAÇÃO #
########################

# 1. Criando a matriz de correlação
corr_mtx <-
  cor(
    attrition %>% select_if(is.numeric) %>%
      select(
        Age,
        MonthlyIncome,
        YearsAtCompany,
        TotalWorkingYears
      ) %>%
      na.omit,
    use = "complete.obs"
  )

corr_mtx

# 2. Heatmap para matriz correla;cão
hchart(corr_mtx, animation = FALSE) %>%
  # hc_title(text = "Heatmap da matriz de correlação") %>%
  hc_size(height = 450)


# 3. Outro heatmap para matriz de correlação
corrplot::corrplot.mixed(
  corr =
    corr_mtx,
  upper = "pie",
  tl.pos = "lt"
)


################
# SCATTER PLOT #
################

# Fazendo um subset dos dados para então criar o gráfico
df_scatter <- attrition %>% filter(Department != "Human Resources") %>% sample_frac(.4) 

# 1. Usando a função padrão do R
plot(df_scatter$Age, df_scatter$MonthlyIncome)

# 2. Relacionar Idade com Salário (MonthlyIncome), utilizando uma amostra de 40% dos dados
hchart(df_scatter,
       "scatter",
       hcaes(x = "Age", y = "MonthlyIncome" , group = "Department")) %>%
  hc_title(text = "Age vs Monthly Income") %>%
  hc_subtitle(text = "Relations by Employee's Departments")
