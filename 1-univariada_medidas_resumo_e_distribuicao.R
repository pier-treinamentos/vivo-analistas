library(readr)
library(dplyr)

# Lendo os dados da web
attrition <- read_csv("https://gitlab.com/dados/open/raw/master/ibm_hr_emplyee_attrition.csv") 

# Lendo os dados localmente
# attrition <- read_csv("data/attrition.csv") 
 
##################
# Medidas Resumo #
##################
# Referência externa para estudo: https://rpubs.com/EstatBasica/Cap3

# 1. Calcular a média, mediana, máximo e mínimo de idade dos colaboradores (variável "Age").
mean(attrition$Age)
median(attrition$Age)
max(attrition$Age)
min(attrition$Age)

# 2. Calcular o primeiro e o terceiro quartis
quantile(attrition$Age,.25) # primeito quartil
quantile(attrition$Age,.75) # terceiro quartil

# 3. Calcular todas as estatísticas anteriores para a variável "Age" com a função summary()
summary(attrition$Age)

## EXERCÍCIO 1
# Replicar os cálculos do exemplo "1" para a variável de tempo de trabalho
# na empresa ("YearsAtCompany"). 






# 4. Usando a função summary() para olhar para todas as variáveis do conjunto de dados
summary(attrition)


## CURIOSIDADE -> o R não tem função específica para calcular moda, então vamos criar uma:
moda<-function(x){return(names(sort(-table(as.vector(x))))[1])}


# 5. Aplicando a função criada para encontrar a idade mais frequente
moda(attrition$Age)

########################
# Medidas de dispersão #
########################
# 1. Calcular variância e desvio-padrão do número de empresas já trabalhadas
var(attrition$NumCompaniesWorked)
sd(attrition$NumCompaniesWorked)


#####################################
# Histograma e gráfico de densidade #
#####################################

# 1. Usando funções nativas do R
#  a) Histograma
hist(attrition$DistanceFromHome)

#  b) Densidade
plot(density(attrition$DistanceFromHome), 
     main = "Distance from home (km)")

# 2. Usando o pacote "highcharter"
library(highcharter)
#  a) Histograma
hchart(hist(attrition$DistanceFromHome), showInLegend = FALSE) %>% 
  hc_title(text = "Distance from home (km)")
  
#  b) Densidade
hchart(density(attrition$DistanceFromHome), type = "area", showInLegend = FALSE) %>% 
  hc_title(text = "Distance from home (km)")


## EXERCÍCIO 2
# Crie um histograma e um gráfico de densidade para a variável "MonthlyIncome"


###########
# QUANTIS #
###########

# 1. Mostrar os quartis de "Distância do trabalho"
quantile(attrition$DistanceFromHome, 0:4/4)

## EXERCÍCIO 3
# Calcular os decis de "MonthlyIncome"





############
# Boxplots #
############

# 1. Usando função tradicional do R
boxplot(attrition$MonthlyIncome)

# 2. Usando highcharter
hcboxplot(
  x = attrition$MonthlyIncome  ,
  outliers = TRUE, # Testar com TRUE e FALSE (com e sem outliers)
) %>% 
  hc_chart(type = "column") %>%  # to put box vertical
  hc_title(text = "Monthly Income (US$)")

# 3. Inlcuindo uma variável para categorizar
hcboxplot(
  x = attrition$MonthlyIncome  ,
  var = attrition$Attrition,
  outliers = FALSE,
) %>% 
  hc_chart(type = "column") %>%  # to put box vertical
  hc_title(text = "Monthly Income (US$)") %>%
  hc_subtitle(text = "by Attrition (Yes or No)") 

# 4. Inlcuindo uma segunda variável para categorizar
hcboxplot(
  x = attrition$MonthlyIncome  ,
  var = attrition$Attrition,
  var2 = attrition$Department,
  outliers = FALSE
) %>% 
  hc_chart(type = "column") %>%  # to put box vertical
  hc_title(text = "Monthly Income (US$)") %>%
  hc_subtitle(text = "by Attrition (Yes or No) and Department")
  

## EXERCÍCIO 4
# Faça um box-plot que mostre o "MonthlyIncome" por "Attrition" e "PerformanceRating". 
# Tire suas conclusões so gráfico. 

